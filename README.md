# Sapienza Hub
Link disponibili per il sito
- https://sapienzastudents.gitlab.io/sapienza_hub
- https://sapienzahub.it

## Introduzione
Sito non ufficiale della sapienza, creato dagli studenti per gli studenti
Il sito racchiude:

- *Link utili*: per i gruppi Facebook, Telegram, WhatsApp, Discord e Slack tutti creati ed amministrati da studenti e indirizzi email ufficiali della Sapienza

- *FAQ*: raccolta di domande recenti, con la relativa risposta, poste dagli studenti.

- *Tools*: raccolta di tool creati dagli studenti per agevolare la risoluzione di alcuni esercizi di varie materie


## Tecnologie usate
- Bulma CSS, per la parte riguardante la UI (necessaria la conoscenza di sass e scss per modificare il src)
- Vue.js per la costruzione dei Tool

## Come contribuire
- Se si vuole contribuire alla sezione tools bisogna lavorare nella directory "public/tools $NOME_TOOL/". I tool vengono scritti usando Vue.js quindi è l'unico framework JavaScript ammesso. E' possibile anche scrivere i tool in JavaScript puro, ma sarebbe meglio evitare per agevolare la futura manutenzione ed estendibilità del codice.

- Se si vuole contribuire al contenuto delle pagine principali (faq, home, links) bisogna lavorare nella directory "src/" e modificare i soli file .md, successivamente quando si saranno apportare le modifiche si dovrà lanciare lo script build.py che si occuperà della creazione dei rispettivi html rispettando la struttura impostata per il corretto funzionamento del framework BulmaCSS

