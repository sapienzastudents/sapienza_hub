/*
The Bulma package does not come with any JavaScript.
Here is however an implementation example, which toggles the class is-active on both the navbar-burger
and the targeted navbar-menu, in Vanilla Javascript. 
*/
var toggle_el;

document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
    var $navmark = document.getElementsByClassName("nav-mark");

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
  
      // Add a click event on each of them
      $navbarBurgers.forEach( el => {
        toggle_el = el;
        el.addEventListener('click', () => {
          
          // Get the target from the "data-target" attribute
          const target = el.dataset.target;
          const $target = document.getElementById(target);
  
          // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
          el.classList.toggle('is-active');
          $target.classList.toggle('is-active');
  
        });
      });

      // My code to autoclose dropdown in mobile view (to improve in future)
      for(let i=0;i<$navmark.length;i++){
        $navmark[i].addEventListener("click", function(){
            let navex = document.getElementById("navbarBasicExample");
            navex.classList.toggle("is-active");
            toggle_el.classList.toggle("is-active");
        });
      }      
    }
  });
