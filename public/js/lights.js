var light = document.getElementById("lights");
var swt = document.getElementById("toggle_switch");
var lstatus = document.getElementById("lstatus");
  
// This function set a cookie about theme preference
function setCookie(nome, valore){
    document.cookie = nome + "=" + valore + "; ";
}

// This function get a cookie about theme preference
function getCookie(nome) {
    var name = nome + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

// Set global theme
function setTheme(nome){
    if(nome.includes("dark")){
        light.textContent = "Lights off";
        theme.href = "/sapienzahub/css/self/dark_theme.css";
        setCookie("curr_theme","dark");
    }
    else if(nome.includes("light")){
        light.textContent = "Lights on";
        theme.href = "/sapienzahub/css/blog.css";
        setCookie("curr_theme","light");
    }
}

// On document load read cookie and set appropriate theme
document.addEventListener("DOMContentLoaded",function(){
    var curr = getCookie("curr_theme");
    if (curr.includes("light")){
         document.getElementById("lstatus").checked = true;
         setCookie("curr_theme","light");
         setTheme("light");
     }
     else{
         document.getElementById("lstatus").checked = false;
         setCookie("curr_theme","dark");
         setTheme("dark");
     }   
 });

// On switch interaction change current theme
 swt.addEventListener("click", function(){
    lstatus.checked  ? setTheme("light") : setTheme("dark");
  });
