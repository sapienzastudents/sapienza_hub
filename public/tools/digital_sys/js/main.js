/*---------------------------------
CONTROLS FOR BASE FORMAT AND 
ERROR HANDLING
---------------------------------*/

$(".base").on("blur",function(){
    var $this = $(this); 
    if($this.val().length==0){
        $this.next().html('<img class="img_err" src="resources/img/signal/warning.svg"/> ERROR! the field cannot be empty');
        $this.addClass("err");
        view_legend();
    }
    else if(isNaN($this.val())){
        $this.next().html('<img class="img_err" src="resources/img/signal/warning.svg"/>ERROR! the field must contains a number');
        //$this.next().text("ERROR! the field must contains a number");
        $this.addClass("err");
    }
    else if($this.val() < 2 || $this.val() >16 ){
        $this.next().html('<img class="img_err" src="resources/img/signal/warning.svg"/>ERROR! the number must be higher than 2 and lower than 17');
        $this.addClass("err");
    }
    else{
        $this.next().html("");
        $this.removeClass("err");
    }
});


/*---------------------------------
CONTROLS FOR NUMBER FORMAT AND 
ERROR HANDLING
---------------------------------*/
$(".num").on("mouseout",function(){
    var $from = $("#from");
    var $bfrom = $("#basefrom");
    var $bto = $("#baseto");

    if($bfrom.val()!=16 && isNaN($from.val())){
        //TODO: Display error message
        $from.addClass("err"); 
    }

    //Check if one of base is incorrect
    else if($bfrom.hasClass("err") || $bto.hasClass("err")){
        $from.addClass("err");
    }

    else if($from.hasClass("err")){
        $from.removeClass("err");
    }
});

//NOT WORKING TRY WITH REGEX
/*---------------------------------
CONTROLS IF NUMBER TO CONVERT 
RESPECT BASE (EXAMPLE 12 ISN'T IN BASE 2)
---------------------------------*/
$(".num").on("input", function(){
    var $bfrom = $("#basefrom");
    var $from = $("#from");
    var temp = Number($from.includes($bfrom));

    if(!$bfrom.hasClass("err") && !$from.hasClass("err") && temp>=$bfrom){
        $from.addClass("err");
    }

    else if($from.hasClass("err") && !$from.includes($bfrom)){

    }
});

/*---------------------------------
BASE CONVERTION FUNCTION
---------------------------------*/
$("#conv").on("click",function(){
    var $from = $("#from");
    var $to = $("#to");
    var $bfrom = $("#basefrom");
    var $bto = $("#baseto");
    var $ca2 = $("#ca2").prop("checked"); //Check if Ca2 option is checked

    //CHECK IF FROM VALUE IS EMPTY OR CONTAIN SPACE AND ADD ERROR CLASS
    if($from.val()=="" || $from.val().includes(" ")){
        $from.addClass("err");
    }

    if($from.hasClass("err")){
        alert("Ops! we have a problem, please correct the field with red border :(");
        $to.val("");
    }
    else{
        var toRet = ""; //THIS RAPRESENT THE FINAL VALUE
    
        if($bfrom.val()==2 && $ca2){

            //CHECK IF THE NUMBER IS NEGATIVE
            if($from.val()[0]=="1"){
                var res = $from.val()[0]*Math.pow(2,$from.val().length-1) * -1;
                var count = $from.val().length-2;

                //POLYNOMIAL METHOD
                for(var i=1; i<$from.val().length; i++){
                    res += $from.val()[i]*Math.pow(2,count);
                    count--;
                }
                toRet = res;
            }
        }
        
        else if($bto.val()==2 && $bfrom.val()!=2 && $from.val()<0 && $ca2) { 

            var res = parseInt($from.val() + '', $bfrom.val()).toString(2).split("");
            
            for(var i=0;i<res.length;i++){
                res[i] = res[i]=="0"? "1" : "0";
            }
            res = Number(res.join(""));
            
            toRet = parseInt((res+1) + '', 10).toString(2);
        }

        else {
            toRet = parseInt($from.val() + '', $bfrom.val()).toString($bto.val());
            
            /*IF CA2 IS ACTIVE, THE INITIAL NUMBER IS POSITIVE AND THE RESULT START WITH 1
              (WHICH MEANS IS NEGATIVE) FIX IT, ADDING A 0 AS FIRST NUMBER
            */
            if($ca2 && $from.val()>0 && toRet[0]=="1"){
                toRet = "0"+toRet;
            }
        }

        $("#to").val(toRet);
    }
});

/*---------------------------------
FUNCTION FOR OPERATIONS (SUM, SUB, MUL, DIV)
---------------------------------*/
$(".op").on("click",function(){

    var $bop = $("#baseop");
    var $op1 = Number(parseInt($("#op1").val() + '', $("#baseop").val()).toString(10));
    var $op2 = Number(parseInt($("#op2").val() + '', $("#baseop").val()).toString(10));
    var clk = $(this).attr("id");
    var sp; // Result of operation

    if($("#baseop").hasClass("err")){
        //SEGNALA L'ERRORE
        $("#op1").addClass("err");
        $("#op2").addClass("err");
        $("#result").val("You wrong something, check the error");
    }

    //MANAGE SUM OPERATION
    else if(clk=="sum"){
        sp = parseInt(($op1+$op2) + '', 10).toString($bop.val());
        $("#result").val(sp);
    }
    //MANAGE SUB OPERATION
    else if (clk=="sub"){
        sp = parseInt(($op1-$op2) + '', 10).toString($bop.val());
        $("#result").val(sp);
    }
    //MANAGE MUL OPERATION
    else if (clk=="mul"){
    let sp = parseInt(($op1*$op2) + '', 10).toString($("#baseop").val());
       $("#result").val(sp);
    }
    
    //MANAGE DIV OPERATION
    else{
       let sp = parseInt(($op1/$op2) + '', 10).toString($("#baseop").val());
       $("#result").val(sp);
    }
});


/*---------------------------------
ERROR POP-UP BEHAVIOR
---------------------------------*/
$("#err_bttn").on("click",function(e){
    e.preventDefault();
    $("#err_view").css("display","none");
});


function view_legend(){
    $("#err_view").css("display","block");
}
