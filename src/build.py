#pip install markdown2
#pypm install markdown2      # if you use ActivePython (activestate.com/activepython)
#easy_install markdown2      # if this is the best you have

import markdown2

def addClass(target,origin,new):
    while (target.find(origin)!=-1):
            target = target.replace(origin, new)
    return target
    

def genFAQ(filename):
    #Open and read markdown file
    f_in = open(filename, "r")
    txt = f_in.read()
    f_in.close()

    #Open and write to html file
    out = open("../public/pages/faq.part.html","w")
    cards = txt.split("<footer>")[0].split("<hr/>") # Every card repr question+answer

    res = '<link rel="stylesheet" href="css/faq.css">'
    for i in range(len(cards)):
        q,a = markdown2.markdown(cards[i].strip()).split("</h3>")
        
        #Add class to specific element (need this because markdown2 support html-classes extra for few elements)    
        a = addClass(a,"<p>","<p class ='is-size-5'>") 
        a = addClass(a,"<li>","<li class ='is-size-5'>")

        res = res + '<div class="card"><div class="card-content"><div class="content">' + q
        res = res + '</h3><hr noshade>' + a + "</div></div></div>"
    res = res + '<footer class="footer"><div class="content has-text-centered"><p>L\'informazione che cercavi non è presente? segnalacela <a href="https://forms.gle/sUYyCSWSCvZmnqnk9">Qui</a> e provvederemo a trovare la risposta ed inserirla nella lista =)</p></div></footer>'
    out.write(res)
    out.close()

def genLinks(filename):
    #Open and read markdown file
    f_in = open(filename, "r")
    txt = f_in.read()
    f_in.close()

    #Open and write to html file
    out = open("../public/pages/links.part.html","w")
    
    txt = markdown2.markdown(txt)
    txt = addClass(txt,"<p>","<p class ='is-size-5'>") 
    txt = addClass(txt,"<li>","<li class ='is-size-5'>")

    res = '<div class="content">' + markdown2.markdown(txt) + "</div>"
    out.write(res)
    out.close()

print("Generate HTML files...")
genFAQ("faq.md")
print("faq\t\tOK")
genLinks("links.md")
print("links\t\tOK")
