### Come funziona il calcolo del voto finale (voto di laurea)?

**Il voto finale è a completa discrezione della commissione di laurea**. Tuttavia esistono delle
linee guida che la commissione si è auto-imposta per normare i voti dati. In breve:

1. Il punteggio di partenza è la media ponderata, rapportata a 110 (ovvero `(x/30)*110`)
2. A tale media si aggiungono, se si ha diritto:
   * **[TRIENNALE]** 3 punti se lo studente si laurea in corso, 2 se entro Marzo oppure 1 se entro il primo anno
   fuori corso
   * **[TRIENNALE]** 1 punto se si ha la media di almeno 27
   * **[TRIENNALE]** 1 punto se ha almeno tre lodi oppure ha partecipato al programma Erasmus
   * **[MAGISTRALE]** 1 punto se ci si laurea entro 28 mesi dall'iscrizione
   * **[MAGISTRALE]** 1 punto se si è partecipato al programma Erasmus
3. Al voto così calcolato si possono aggiungere da 0 a 9 punti a discrezione della commissione
di laurea.

Nota che l'incremento massimo (per il punto 2 e 3 sommati) è pari a 13 punti per la **triennale** e 11 per la **magistrale**.

[Per ulteriori dettagli fare riferimento al documento presente sul sito ufficiale](https://www.studiareinformatica.uniroma1.it/sites/default/files/Calcolo%20voto-laurea%20.pdf).

<hr/>

### Che differenza c'è tra tirocinio e tesi?

La terminologia corretta è **tirocinio** per la triennale (con presentazione
della *relazione di tirocinio*) e **tesi** per la magistrale (con presentazione della vera e
propria *tesi*).

<hr/>

### Posso fare un tirocinio in azienda?

Si! Non c'è alcuna differenza tra un tirocinio interno o esterno. Un tirocinio esterno prevede la
nomina di un *tutor* interno (futuro *relatore*), ovvero di un professore che seguirà il lavoro
dello studente con riunioni periodiche, e di un *referente* all'interno dell'azienda, che dovrà
seguire lo studente nella fase di tirocinio.

Si possono trovare le offerte di tirocinio in diversi modi:

* Tramite la piattaforma JobSoul
* Tramite gli eventi in Sapienza, come l'ITMeeting
* Tramite proprie ricerche

Qualora l'azienda non dovesse essere essere presente su JobSoul, si può iscrivere in poco tempo.
[Ulteriori informazioni si trovano nella pagina relativa ai tirocini sul sito ufficiale.](https://www.studiareinformatica.uniroma1.it/laurearsi/regolamento-tirocinio)

<hr/>

### Posso farmi riconoscere un tirocinio già svolto in azienda?

Di norma, il tirocinio può essere iniziato dopo il completamento dei 130 CFU minimi, e dopo
le procedure burocratiche relative alla piattaforma JobSoul. Tuttavia, per chiarimenti relativi
al caso specifico si può consultare il docente responsabile indicato [nella pagina del regolamento del tirocinio](https://www.studiareinformatica.uniroma1.it/laurearsi/regolamento-tirocinio).

<hr/>

### Per quali questioni mi devo rivolgere alla Commissione Gestione della Didattica (CGD) per quali alla segreteria studenti e per quali direttamente ai docenti?

La CGD gestisce le problematiche relative a:

   1. piani di studio
   2. abbreviazioni di corso (riconoscimento crediti a fronte di trasferimenti, per studenti già laureati, ecc.)
   3. domande di studenti (sostituzione di esami non più attivi, Erasmus, ecc.)

L'istruzione delle pratiche di abbreviazione di corso e riconoscimento crediti (anche di quelle richieste in modo informale, prima di immatricolazione o trasferimento) è curata dalla [Prof.ssa Lombardi](mailto:mara.lombardi@uniroma1.it) e dal [Prof. Bonifazi](giuseppe.bonifazi@uniroma1.it).
La verifica dei requisiti di ammissione alla laurea magistrale è curata dal [Prof. Sciarra](giulio.sciarra@uniroma1.it).
L'istruzione dei piani di studio è curata dalla [Prof.ssa Lombardi](mara.lombardi@uniroma1.it).
La predisposizione dell’orario è curata dalla Presidenza della Facoltà, con la collaborazione della [Prof.ssa Lombardi](mara.lombardi@uniroma1.it).
L'organizzazione delle sedute di laurea è curata dal [Prof. Garzia](fabio.garzia@uniroma1.it)


La Segreteria studenti gestisce le problematiche relative a:
   
   1. procedure di immatricolazione e iscrizione agli anni successivi e relative scadenze
   2. procedure di trasferimento da altri corsi di laurea o da altri Atenei
   3. accoglimento delle domande di abbreviazione di corso (riconoscimento crediti, ecc.) e delle domande di studenti in genere (es. part time)
   4. accoglimento delle domande di laurea ed istruzione delle relative pratiche

La segreteria studenti della Facoltà di Ingegneria Civile e Industriale è in Città Universitaria (ingresso di Viale Regina Elena). Per gli orari di apertura e recapiti consultare il [sito della Sapienza](http://www.uniroma1.it/didattica/sportelli/segreterie/segreteria-studenti-di-ingegneria)


I docenti gestiscono le problematiche relative a:

   1. attività didattica del corso
   2. materiale didattico
   3. appelli di esame

In linea di massima, gli indirizzi e-mail sono nel formato nome.cognome@uniroma1.it; i numeri telefonici si possono rintracciare sul [sito della Sapienza](http://www.uniroma1.it/contatti). 

<hr/>

### Dove trovo il bando relativo agli assegni di ricerca?
I bandi degli assegni di ricerca vengono pubblicati a cura dell’ufficio concorsi sulla pagina web Bandi e Concorsi

Quand’è che il Settore interviene per recuperare un credito dell’Amministrazione?
In caso di esito negativo del tentato recupero stragiudiziale del credito, avviato dapprima da un Dipartimento o comunque ad opera di una struttura periferica dell’Ateneo e successivamente tramite il Settore Recupero Crediti, gli avvocati in servizio presso il Settore Contenzioso Civile, Tributario e Amministrativo-Contabile potranno essere incaricati dell’esperimento, o meno, di ogni utile iniziativa giudiziale (ad es.: ricorso per ingiunzione o atto di citazione, etc.).

<hr/>

### Quale è il termine di prescrizione per recuperare un credito?
In genere, la prescrizione per il recupero di un credito (ad esempio di natura contrattuale) è quella c.d. ordinaria ossia decennale (cfr art. 2946 c.c.).
Fanno eccezione, i casi di prescrizioni cc.dd. “brevi” e “presuntive”, diverse tra loro a seconda della natura del diritto vantato e disciplinate dagli artt. 2947 e ss del codice civile (quinquennali, triennali, biennali, annuali e c.d. infra annuali che maturano cioè con il decorso di meno di dodici mesi).
Perché un diritto di credito, non si estingua occorre, quindi, che venga interrotto in maniera regolare il decorso temporale della prescrizione, ad esempio, con l’invio di una idonea diffida ad adempiere tramite R.A.R. o P.E.C. presso la residenza (se persona fisica) o la sede legale (se persona giuridica) o presso l’eventuale indirizzo ufficiale di P.E.C. risultante dai pubblici registri.

<hr/>

### Quale è la procedura da seguire se viene notificato a cura di una controparte un ricorso per ingiunzione e pedissequo decreto ingiuntivo e/o un atto di precetto c/o una struttura periferica (non centrale) dell’Università?
Affinchè sia correttamente eseguita occorre che la notifica avvenga presso la sede legale dell’Ateneo, come noto, sita in Roma, Piazzale Aldo Moro n.5 o presso l’indirizzo P.E.C. ufficiale risultante dall’apposito registro: protocollosapienza@cert.uniroma1.it
Qualsiasi notifica effettuata c/o altro indirizzo o sito P.E.C. non ufficiale è irregolare.
Laddove vengano eseguite notifiche non regolari, le competenti strutture periferiche dovranno operare come segue:

   - se la sorte rivendicata dovesse risultare dovuta, dovranno contattare direttamente il difensore della controparte prospettando la possibilità di pagare il dovuto fatta eccezione per le spese legali relative al decreto ingiuntivo o al precetto che non andranno corrisposte; ciò contestualmente alla rinuncia da parte dell’ingiungente, da acquisire per iscritto, alle pretese, al diritto, agli atti ed all’azione di cui al monitorio o al precetto; 
   - se la sorte dovesse risultare (anche parzialmente) non dovuta o vi fossero altre eccezioni da far valere, dovranno rimettere prontamente l’atto (decreto ingiuntivo o precetto) irritualmente notificato ad ARAL che provvederà ad effettuare e comunicare le proprie valutazioni sul caso. 

<hr/>

### Entro quanto tempo si deve pagare un decreto ingiuntivo notificato a cura di una controparte contro l’Amministrazione?
In caso di notifica di un provvedimento monitorio (decreto ingiuntivo) che non si possa o intenda opporre, ai fini del pagamento occorre distinguere due casi:

- se il provvedimento monitorio è dotato di provvisoria esecuzione, il pagamento va effettuato senza dilazione, al fine di evitare che il decorso del tempo comporti aumenti di interessi e di costi; 
- se il provvedimento monitorio non è dotato di provvisoria esecuzione, il pagamento può essere effettuato entro 40 gg dalla notifica, ferma restando l’opportunità di corrispondere con tempestività la somma dovuta, al fine di evitare che il decorso del tempo comporti pure in tal ipotesi aumenti di interessi e di costi. 

<hr/>
    
### Entro quanto tempo si deve pagare un atto di precetto notificato contro l’Amministrazione a cura di una controparte?
In caso di notifica di un atto di precetto avverso cui non si possa o intenda proporre opposizione, occorre effettuare il relativo pagamento entro il termine (in genere) di 10 gg, al fine di evitare in mancanza l’inizio dell’esecuzione forzata (ad es., pignoramento), con conseguente aumento di interessi e costi.

<hr/>

### Entro quanto tempo si deve opporre un decreto ingiuntivo notificato a cura di una controparte contro l’Amministrazione?
Il termine per proporre opposizione avverso un provvedimento monitorio (decreto ingiuntivo) è di 40 gg dalla notifica (cfr art. 641 c.p.c.).
Poiché entro tale termine ARAL dovrà perentoriamente proporre opposizione, è indispensabile che il Dipartimento o la struttura amministrativa coinvolti facciano pervenire con congruo anticipo agli avvocati interni incaricati una dettagliata relazione corredata dalla documentazione completa utili per la redazione dell’atto di opposizione.

<hr/>

### Entro quanto tempo si deve opporre un atto di precetto notificato a cura di una controparte contro l’Amministrazione?
L’opposizione avverso un atto di precetto va proposta a seconda dei casi:

- perentoriamente, entro 20 gg dalla notifica del precetto qualora se ne contesti la regolarità formale (cfr art. 617 c.p.c. I comma) (ad es.: errore nei conteggi); 
- senza termine, qualora si contesti il diritto della parte istante a procedere all’esecuzione forzata e questa, però, non sia ancora iniziata (cfr art. 615 c.p.c. I comma); 
  in tale ipotesi 
        - ossia laddove si intenda contestare radicalmente la fondatezza stessa dell’esecuzione forzata (ad es., per la mancanza di un titolo esecutivo, o erroneità del soggetto debitore, etc.) 
        - sarà in ogni caso opportuno proporre senza indugio l’opposizione al fine di evitare il decorso del termine dilatorio di 10 gg per l’inizio dell’esecuzione forzata (ad es., pignoramento), con conseguente necessità di dover opporre anche quest’ultima, con aggravio di costi.

Anche in tali casi è indispensabile che il Dipartimento o la struttura amministrativa coinvolti facciano pervenire con la massima tempestività agli avvocati interni incaricati di proporre opposizione una dettagliata relazione corredata dalla documentazione completa utili per la redazione dell’atto di opposizione. 

<hr/>

### In quale forma debbono essere trasmessi ad A.R.A.L., al fine di attivare l’azione di recupero, i libri o i registri dell’Amministrazione attestanti un credito dell’Università nei confronti di terzi?
Al fine di evitare possibili eccezioni sia da parte dei giudici che delle controparti in sede giudiziale, è preferibile che tutti i libri ed i registri che costituiscono le prove scritte dei crediti dell’Amministrazione universitaria avverso terzi e su cui si fondi l’azione di recupero, rechino in calce l’attestazione da parte di un funzionario all’uopo autorizzato o di un notaio che ne attesti la regolare tenuta a norma delle leggi e dei regolamenti ai sensi e per gli effetti dell’art. 635 c.p.c..

<hr/>

### Come deve essere redatta una relazione istruttoria richiesta dagli avvocati per la difesa dell’amministrazione in una causa?
All’atto del conferimento di un incarico, gli avvocati interni, in genere, provvedono a richiedere alla struttura dell’Ateneo a conoscenza dei fatti di causa la trasmissione di una relazione scritta, al fine di adempiere nel miglior modo al proprio mandato difensivo nell’interesse dell’Amministrazione.
A tale scopo, è necessario che la relazione sia redatta dalla competente struttura nel modo più dettagliato possibile, indicando specificamente ed in maniera chiara i fatti di cui si discute, con l’allegazione di tutta la relativa documentazione atta a provare quanto sostenuto e con l’indicazione di eventuali testimoni disposti a confermare (in giudizio) quanto sostenuto dall’Amministrazione.

<hr/>

### Si può diffidare o essere diffidati via P.E.C.?
Sì. Una diffida ad adempiere inoltrata o ricevuta presso l’indirizzo ufficiale di P.E.C. risultante dai pubblici registri (REGINDE o INI.PEC) ha ex lege il medesimo valore di una formale diffida inoltrata o ricevuta tramite racc. a.r. presso la residenza (se persona fisica) o la sede legale (se persona giuridica).
Alla luce di quanto precede - poiché già la sola mera ricezione da parte del mittente degli avvisi di accettazione e della successiva conferma rendono legalmente perfezionato l’inoltro della P.E.C. nei confronti del destinatario (equivalendo a conoscibilità, senza rilevare de facto che quest’ultimo ne possa quindi avere avuto o meno l’effettiva conoscenza) - è onere di ogni singolo titolare monitorare quotidianamente l’eventuale indirizzo di P.E.C. assegnatogli dall’Amministrazione, al fine di evitare la maturazione di malaugurati effetti pregiudizievoli in danno dell’Ateneo.


<footer>

L'informazione che cercavi non è presente? segnalacela [Qui](https://forms.gle/sUYyCSWSCvZmnqnk9) e provvederemo a trovare la risposta ed inserirla nella lista =)
