### Gruppi e links

Gruppi

* [Gruppo Facebook](https://m.facebook.com/groups/informaticasapienza) ufficioso per gli studenti della facoltà
* [Canale Telegram](https://t.me/sapienzafeed) con Informazioni e Avvisi riguardanti la facoltà, i professori e i corsi
* [Gruppo Telegram](https://t.me/sapienzainformatica) della Facoltà di Informatica (Triennale)
* [Gruppo Telegram](https://t.me/InformaticaSapienzaTeledidattica) della Facoltà di Informatica via telematica (Unitelma)
* [Server Discord](https://discord.gg/YxENrKk) per esercizi e studio di gruppo
* [Workspace Slack](https://ecssap.slack.com/) per gli studenti di Teledidattica "Unitelma"
* [Pagina GitLab](https://gitlab.com/sapienzastudents/exercises) dove sono hostate esercitazioni
* [Pagina GitLab](https://gitlab.com/sapienzastudents/papers) dove sono hostati gli appunti
* [Gruppo Telegram](https://t.me/infostudapp) per il beta testing dell'app di Infostud

Informazioni importanti

* [Regolamenti e documenti per laurearsi (tirocini e tesi)](https://www.studiareinformatica.uniroma1.it/laurearsi)
* [Regolamento per i tirocini triennali](https://www.studiareinformatica.uniroma1.it/laurearsi/regolamento-tirocinio)

Indirizzi email ufficiali:

* [Email Segreteria Didattica](mailto:segr.didattica@di.uniroma1.it), per domande relative ad esami, corsi, cambi canale, etc.
* [Email Segreteria Studenti](mailto:segrstudenti.I3S@uniroma1.it), per domande relative a Infostud, Tasse, Bollettini etc.
* [Email supporto tecnico Infostud](mailto:infostud@uniroma1.it) per segnalare malfunzionamenti o eventuali problemi.

### Quale segreteria?

Esistono 3 segreterie per uno studente (vale per quasi tutti i dipartimenti Sapienza):

1. **Segreteria (amministrativa) studenti** (di facoltà, ovvero I3S): a questa segreteria ci si può rivolgere per problemi verso l'Ateneo, ovvero:
   - Tasse e pagamenti
   - Appelli laurea non visibili
   - Part-time, iscrizione, cambio corso
   - Problemi con Infostud (tipo account bloccato, ISEE errato, etc)
   - Ritiro pergamena di laurea o documenti sullo status di studente
   - Altro, che riguarda però l'Ateneo

2. **Segreteria didattica** (di corso/dipartimento): a questa segreteria ci si può rivolgere per problemi di didattica, ad esempio:
   - Problemi con l'erogazione dei corsi
   - Cambi canale
   - Modulistica per la laurea
   - Orari delle lezioni / aule / etc
   - Altro, che riguarda però il dipartimento o i corsi del dipartimento (triennale in Informatica, magistrale in CS e DS)

3. **Segreteria amministrativa** (di dipartimento, ovvero DI): è la segreteria che si occupa dell'amministrazione (specie economica) del dipartimento. Raramente uno studente si rivolge alla segreteria amministrativa. Infatti i motivi sono:
   - Rimborso per "missioni" (ovvero trasferte per conferenze, convegni, etc)
   - Burocrazia per il proprio status di dipendente del dipartimento (quindi **non** riguarda gli studenti)
   - Parte economica e amministrativa per le **borse di studio e collaborazione**, assegni di ricerca e quant'altro, **ma solo quelli erogati dal dipartimento** (ovvero, NON LazioDisco e altre)

**Nota importante**: la 1 e la 3 sono diverse, anche se il nome della prima può confondersi con la seconda. Dunque, quando qualcuno vi dice di andare in segreteria amministrativa, verificate esattamente quale sta indicando in quel momento.

Per informazioni sugli orari:

- **Segreteria studenti**: [consulta la sezione sul sito dell'Ateneo](https://www.uniroma1.it/it/pagina/segreteria-di-ingegneria-dellinformazione-informatica-e-statistica)
- **Segreteria didattica**: [consulta la sezione sul sito del corso di studio](https://www.studiareinformatica.uniroma1.it/segreteria-didattica)
- **Segreteria amministrativa**: in via Salaria, di fianco alla segreteria didattica. In genere si viene convocati per le borse e altro, non ci sono orari per ricevimento pubblico.

